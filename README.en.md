# MindText

## Introduction

MindText is an open source text toolbox based on MindSpore.

The master branch works with **MindSPore 1.2**.

## License

This project is released under the [Apache 2.0 license](LICENSE).

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mindspore/mindtext/issues).

## Acknowledgement

MindSpore is an open source project that welcome any contribution and feedback.
We wish that the toolbox and benchmark could serve the growing research
community by providing a flexible as well as standardized toolkit to reimplement existing methods
and develop their own new semantic segmentation methods.

# Contributor

Phantom(947291834@qq.com)
Minjie Hu(1249213249@qq.com)
xiaoyisd(524925587@qq.com)
Norato(834184466@qq.com)
LiHuamao123(LiHuamao123@foxmail.com)
bridge(1131156501@qq.com)
ChanJiatao(chenjiatao777@163.com)
LiuHongYe(470141216@qq.com)
tsunami(1905470093@qq.com)
ChiJunlong(2869897445@qq.com)
pallidlight(965504148@qq.com)
TsaiKunzhao(562552741@qq.com)
DingMingHao(294270681@qq.com)
caijian(937747918@qq.com)
HuangZhengBo(1219326125@qq.com)
huangsonglong(2802993151@qq.com)
txzskywalker(73560446@qq.com)
chenhuamin9527(359247454@qq.com)
miaoziming(402720769@qq.com)
dongjianyu(1325895127@qq.com)
Carina233(910780586@qq.com)
LunarCM(708827045@qq.com)
xay2001(1602357810@qq.com)
xiaoyuzhau(1042958277@qq.com)
lijianming(lijianming666666@163.com)
ChenXuechen(312117408@qq.com)
jmsyyds(1535276562@qq.com)
xiaoyuzhau(1042958277@qq.com)
ChenXuechen(312117408@qq.com)
Hamilton北（1209233513@qq.com）
huanghonghao(373658829@qq.com)
linkaizhe(499780002@qq.com)
Divine(2538048363@qq.com)
Caixugang(309496562@qq.com)
LiangRio(2453372256@qq.com)
Mavendetta985(1373086696@qq.com)
Rayen227(16607615570@163.com)
jamesonleong(jamesonleong@qq.com)
Liyanlong(2373515564@qq.com)
MChloe(2284161124@qq.com)
M-Zif(lucas011014@qq.com)
Rayen227(16607615570@163.com)
7aY1 (389354380@qq.com)
zhangjiadong(2628961452@qq.com)
Swing9912(843678794@qq.com)
Lee_rvr(741347759@qq.com)
Longhaolin(2916691149@qq.com)
XingXing(2407116610@qq.com)
BaYan01(1667041026@qq.com)
Yuzi(953984595@qq.com)
ZengWei(sticktoonething@163.com)
Liyanlong(2373515564@qq.com)
Soleil(1272134437@qq.com)
chenyue2021(1294308924@qq.com)
wengweihua(2507549972@qq.com)
jingshoupolan(1771442206@qq.com)
Akitsu(963327756@qq.com)
loomt(763058991@qq.com)
Chriscy(1546711056@qq.com)
orans3（1484367181@qq.com）
scnulinkai(1043269332@qq.com)
ntong2021(1990296278@qq.com)
xiangjingyang(jingyangxiang@foxmail.com)
zhibing-zzy(1470603076@qq.com)
JiaY Huang(625723003@qq.com)
WuJianhui(1091538455@qq.com)
FengLingCong(flclizhi@163.com)
Qinzhenghan(1079913845@qq.com)
liangyuting(1909558284@qq.com)
Tujiachen(1762957373@qq.com)
likeshu(1094462380@qq.com)
zhuangzhihao(1559452919@qq.com)
LiangHongYuan(1633667497@qq.com)
xglovecode(491705331@qq.com)
gggagster(614580586@qq.com)
SongYiYun(1493502917@qq.com)
Joyce(2013520960@qq.com)
ZhangJiaLuo(2916063350@qq.com)
jia_zhaoyu(jzy0706@126.com)
LiuYichen(2771875363@qq.com)
wzj-jgt(1035434544@qq.com)
chen_zhi_wei(1561167298@qq.com)
Master-bige（1532546744@qq.com)
DestinyOfICE(626068058@qq.com)
YxXian(850615324@qq.com)
Master-bige（1532546744@qq.com)）
lijiazhan(957794003@qq.com)
wangfengjian(fjbmx101@163.com)
ohyeahlm(328950115@qq.com)
zhang-yi521(1751626187@qq.com)
ZhongXin(1140091006@qq.com)
Zx(1371981024@qq.com)
Zhanghao(779054448@qq.com)
HeGuoping(494013153@qq.com)
Duanshuzhe(1379894217@qq.com)
lian-yichen(1147842143@qq.com)
PengCheng(2421962762@qq.com)
HuZhiyuan(3050770371@qq.com)
CakeCN(cakebaker.0308@gmail.com)
WanghChensu(634852549@qq.com)
Zhouzijie(13458195923@163.com)
SaigyoujiShizuka(saigyoujishizuka@gmail.com)
Changxiaowei（421870499@qq.com)
Xiejunhong (j1487052830@163.com)
Hongru(230198152@seu.edu.cn)
ZiyuZhang(alfred_torres@163.com)
ZhangCan(269101348@qq.com)

## Citation

If you find this project useful in your research, please consider citing:

```latex
@misc{mindvsion2021,
    title={{MindText}:MindSpore Vision Toolbox and Benchmark},
    author={MindText Contributors},
    howpublished = {\url{https://gitee.com/mindspore/mindtext}},
    year={2021}
}
```
